namespace bdapi_kits.Models
{
  public class Kit
  {
    public int Id { get; set; }
    public string Token { get; set; }
    public string Owner { get; set; }
  }
}
